## Retrospectiva de Iteración 1

En esta iteración se logro iniciar y configurar el proyecto. Se logro cumplir con los objetivos especificados, completando exitosamente los CRUD de productos y sus categorias.

Por el momento se programo para poder asignar solo una imagen al producto dado de alta, lo cual queremos mejorar para permitir multiples imagenes a un producto. Esto lo intentaremos agregar en la etapa de refinamiento

