# Trabajo en equipo

**Lider de Iteración:** Cubilla Mauro

### Distribucion de trabajo

**Cubilla Mauro:** Configuracion inicial del proyecto, crear y programar modelos\clases, controladores y migraciones.

**Pluhator Nicolas:** Desarrollo de interfaz de usuario (vistas), controles de validacion, definicion de rutas, creacion de seeders.

# Diseño OO

### Diagrama de Clases

![Diagrama](Diagrama/i1.png)

# Wireframe y casos de uso

### AdminLayout

![](Diagrama/w1.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w1.jpg"  width="500" >

El Admin Layout estará presente en todas las vistas de administracion. En el sidebar estaran los enlaces a las configuraciones/CRUDS.

En esta iteración estaran los enlaces para
- Lista/CRUD de productos
- Lista/CRUD de categorias de productos

En el area CONTENT es donde estaran las vistas las cuales extiendan del Layout

### Vista: Listado de Productos (Extiende de AdminLayout)

![](Diagrama/w2.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w2.jpg"  width="500" >

### Vista: Listado de Categorías (Extiende de AdminLayout)

![](Diagrama/w3.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w3.jpg"  width="500" >

### Vista: Alta Producto (Extiende de AdminLayout)

![](Diagrama/w4.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w4.jpg"  width="500" >

### Vista: Alta Categoría (Extiende de AdminLayout)

![](Diagrama/w5.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w5.jpg"  width="500" >

### Vista: Editar Producto (Extiende de AdminLayout)

![](Diagrama/w6.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w6.jpg"  width="500" >

### Vista: Editar Categoría (Extiende de AdminLayout)

![](Diagrama/w7.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w7.jpg"  width="500" >

### Vista: Ver Producto (Extiende de AdminLayout)

![](Diagrama/w8.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w8.jpg"  width="500" >

### Vista: Ver Categoría (Extiende de AdminLayout)

![](Diagrama/w9.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w9.jpg"  width="500" >

### Vista: Confirmar Eliminar Producto (Modal)

![](Diagrama/w10.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w10.jpg"  width="500" >

### Vista: Confirmar Eliminar Categoría (Modal)

![](Diagrama/w11.png)
<img src="https://gitlab.com/dantuchi/eshop-poo2/-/raw/main/Iteraci%C3%B3n%201/Diagrama/w11.jpg"  width="500" >

### Caso de uso: Ver listado de productos
1. El usuario accedera a la aplicación web a travez de un navegador.
2. Se le presentará al usuario la página de inicio.
3. En la barra lateral izquierda accedera a **"Productos"**.
4. El sistema traera y mostrara un listado de todos los productos en el sistema.

### Caso de uso: Dar de alta un nuevo producto
Una vez en la pagina de listado de productos (Referirse al caso de uso **Ver listado de productos**).
1. El usuario debera acceder a **"Nuevo"**.
2. Se le presentara al usuario el formulario para rellenar los datos del producto.
3. Una vez completado el relleno de datos se hara click al boton **"Guardar"**.

### Caso de uso: Ver un producto
Una vez en la pagina de listado de productos (Referirse al caso de uso **Ver listado de productos**)
1. El usuario debera acceder a **"Ver"** del producto al cual quiere ver información.
2. Se le presentara al usuario la pagina de informacion del producto seleccionado.

### Caso de uso: Modificar/actualizar un producto
Una vez en la pagina de listado de productos (Referirse al caso de uso **Ver listado de productos**)
1. El usuario debera acceder a **"Editar"** del producto al cual quiere modificar.
2. Se le presentara al usuario el formulario para rellenar los datos del producto ya cargado con los datos actuales.
3. Una vez modificado los datos se hara click al boton **"Guardar"**.

**Caso alternativo:** Desde la vista de ver un producto (Referirse a Caso de uso: Ver un producto)
1. El usuario debera acceder a **"Editar"**
2. Se le presentara al usuario el formulario para rellenar los datos del producto ya cargado con los datos actuales.
3. Una vez modificado los datos se hara click al boton **"Guardar"**.

### Caso de uso: Eliminar un producto
Una vez en la pagina de listado de productos (Referirse al caso de uso **Ver listado de productos**)
1. El usuario debera acceder a **"Eliminar"** del producto al cual quiere eliminar.
2. Se le presentara al usuario una advertencia y consulta para confirmar la eliminacion del producto.
3. El usuario hara click al boton **"Confirmar"**.

### Caso de uso: Ver listado de categorias
1. El usuario accedera a la aplicación web a travez de un navegador.
2. Se le presentará al usuario la página de inicio.
3. En la barra lateral izquierda accedera a **"categorias"**.
4. El sistema traera y mostrara un listado de todos los categorias en el sistema.

### Caso de uso: Dar de alta una nueva categoria
Una vez en la pagina de listado de categoria (Referirse al caso de uso **Ver listado de categoria**).
1. El usuario debera acceder a **"Nuevo"**.
2. Se le presentara al usuario el formulario para rellenar los datos de la categoria.
3. Una vez completado el relleno de datos se hara click al boton **"Guardar"**.

### Caso de uso: Ver una categoria
Una vez en la pagina de listado de categorias (Referirse al caso de uso **Ver listado de categorias**)
1. El usuario debera acceder a **"Ver"** del producto al cual quiere ver información.
2. Se le presentara al usuario la pagina de informacion de la categoria seleccionado.

### Caso de uso: Modificar/actualizar una categoria
Una vez en la pagina de listado de categorias (Referirse al caso de uso **Ver listado de categorias**)
1. El usuario debera acceder a **"Editar"** de la categoria al cual quiere modificar.
2. Se le presentara al usuario el formulario para rellenar los datos de la categoria ya cargado con los datos actuales.
3. Una vez modificado los datos se hara click al boton **"Guardar"**.

**Caso alternativo:** Desde la vista de ver una categoria (Referirse a Caso de uso: Ver una categoria)
1. El usuario debera acceder a **"Editar"**
2. Se le presentara al usuario el formulario para rellenar los datos de la categoria ya cargado con los datos actuales.
3. Una vez modificado los datos se hara click al boton **"Guardar"**.

### Caso de uso: Eliminar una categoria
Una vez en la pagina de listado de categorias (Referirse al caso de uso **Ver listado de categorias**)
1. El usuario debera acceder a **"Eliminar"** de la categoria la cual quiere eliminar.
2. Se le presentara al usuario una advertencia y consulta para confirmar la eliminacion de la categoria.
3. El usuario hara click al boton **"Confirmar"**.





# Backlog de iteraciones

- Como administrador, quiero poder gestionar productos y sus categorías.


# Tareas

- Configuracion inicial del proyecto
- Crear modelos de las clases Producto, Tipo e Imagen
- Crear migraciones de las clases Producto, Tipo e Imagen
- Relacionar los modelos correspondientes
- Crear controladores de las clases Producto, tipo e Imagen
- Crear y configurar rutas
- Crear vista de navegacion inicial
- Crear vistas relacionadas al crud del producto
- Crear seeders para la carga rapida de datos
