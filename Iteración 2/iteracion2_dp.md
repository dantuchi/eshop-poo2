# Trabajo en equipo

**Lider de Iteración:** Cubilla Mauro

# Diseño OO

# Backlog de iteraciones

- Como administrador, quiero poder gestionar personas.
- Como administrador, quiero poder gestionar usuarios.


# Tareas

- Crear modelos de las clases Persona y User
- Crear migraciones de las clases Persona y User
- Relacionar los modelos correspondientes
- Crear controladores de las clases Persona y User
- Crear y configurar rutas
- Crear vistas relacionadas al crud de Persona
- Crear seeders para la carga rapida de datos

