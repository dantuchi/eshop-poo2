# Roadmap

## Iteración 1
Para la iteración 1 queremos desarrollar el módulo de productos que seria el nucleo de nuestro sistema.
En esta entrega el sistema contara con el CRUD de los productos y sus categorias/tipos.

### Historias de usuarios a desarrollar
- Como administrador, quiero poder gestionar productos y sus categorías.

<hr>

## Iteración 2

Para la iteración 2 queremos desarrollar el módulo de personas y usuarios. No trabajeremos la parte de autenticación, solo crearemos la clase usuario e insertaremos manualmente algunos a la base de datos para poder trabajar en la relacion usuario -> persona y a la vez vincularlos a las compras que se trabajara en iteraciones mas adelante.

### Historias de usuarios a desarrollar
- Como administrador, quiero poder gestionar personas.
- Como administrador, quiero poder gestionar usuarios.

<hr>

## Iteración 3

Para la iteración 3 queremos desarrollar el módulo de ingreso de insumos, lo que nos permitira gestionar proveedores y las compras hechas para renovar stock.

### Historias de usuarios a desarrollar
- Como administrador, quiero registrar ingresos nuevos de insumos.
- Como administrador, quiero poder ver estadísticas de compras (insumos).
- Como administrador, quiero poder gestionar proveedores.

<hr>


## Iteración 4

Para la iteración 4 queremos desarrollar el módulo de venta, para poder registrar compras hechas en el local y permitir a los clientes a comprar por medio del sitio web. Tambien se trabajara en el modulo de recomendaciones de productos.

### Historias de usuarios a desarrollar
- Como cliente, quiero buscar un producto para su compra.
- Como cliente, quiero filtrar productos por su categoría.
- Como cliente, quiero agregar productos al carro de compra.
- Como cliente, quiero elegir el método de pago.
- Como cliente, quiero elegir el método de retirar el producto.
- Como cliente, quiero confirmar la compra de mi carro de productos.
- Como administrador, quiero agregar productos a la lista de compra de un cliente
- Como administrador, quiero poder confirmar la compra de productos de clientes.
- Como administrador, quiero poder ver estadísticas de ventas.



<hr>


## Iteración 5
En esta iteración implementaremos la autenticación. Nos enfocaremos en el refinamiento del sistema e intentaremos implementar aquellas historias de usuario que seria bueno que tenga.

### Historias de usuarios a desarrollar

- Como cliente, quiero crearme un usuario.
- Como cliente, quiero logearme a mi cuenta de usuario.
- Como cliente, quiero agregar un producto a mi lista de favoritos
- Como cliente, quiero calificar un producto de 1 a 5
- Como cliente, quiero comentar acerca un producto que compre
- Como cliente, quiero preguntar acerca de un producto
- Como administrador, quiero responder a preguntas de clientes
- Como administrador, quiero ver los diversos movimientos dentro del sistema




