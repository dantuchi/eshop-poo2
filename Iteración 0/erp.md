## Enunciado del problema

Actualmente la empresa realiza una gestión de sus ventas de manera manual donde esto conlleva tiempo y es propenso a errores humano. Por otra parte, la ventas de sus productos se realizan mayormente a los clientes de la misma localidad, limitando la expansión comercial.

## Clientes potenciales

Este sistema tendrá como cliente potenciales todos aquellas empresas o locales que comercializan insumos informáticos.

## Solución propuesta

El sistema ayudará en la automatización de las gestión de las ventas de los productos como también permitirá la expansión del abanico de clientes de la empresa, ya que este será un sistema web, permitiendo la visita de clientes que estén fuera de la localidad de la empresa.

### Requisitos funcionales

Algunas de las actividades que se podrán realizar en el sistema son:

#### Actividades fundamentales:

1. Permitir a los clientes crearse un usuario en el sistema.
2. Permitir a un cliente ya registrado realizar una compra de uno o más insumo informáticos.
3. Permitir que el cliente elija el método de retirar el producto, es decir, envió a domicilio o si retira en el local.
4. Permitir a un cliente realizar búsquedas de productos que desea.
5. Permitir a un empleado de la empresa realizar diferentes ABM's en el sistema.
6. Permitir a un empleado gestionar ventas hechas en el local.
7. Permitir a un empleado registrar la entrada de stock.
8. Permitir a un empleado ver diversas estadísticas.
9. Permitirá al sistema recomendar productos similares a los ultimos vistos por el usuario, con el fin de ofrecer productos similares para facilitar al usuario la búsqueda y elección del producto deseado.

## Historias de usuarios

### Debe tener

- Como cliente, quiero crearme un usuario.
- Como cliente, quiero logearme a mi cuenta de usuario.
- Como cliente, quiero buscar un producto para su compra.
- Como cliente, quiero filtrar productos por su categoría.
- Como cliente, quiero agregar productos al carro de compra.
- Como cliente, quiero elegir el método de pago.
- Como cliente, quiero elegir el método de retirar el producto.
- Como cliente, quiero confirmar la compra de mi carro de productos.
- Como administrador, quiero agregar productos a la lista de compra de un cliente (Compras realizadas en local).
- Como administrador, quiero registrar ingresos nuevos de insumos.
- Como administrador, quiero poder ver estadísticas de ventas.
- Como administrador, quiero poder ver estadísticas de compras (insumos).
- Como administrador, quiero poder confirmar la compra de productos de clientes.
- Como administrador, quiero poder gestionar proveedores.
- Como administrador, quiero poder gestionar personas.
- Como administrador, quiero poder gestionar productos y sus categorías.
- Como administrador, quiero poder gestionar usuarios.

### Sería bueno que tenga

- Como cliente, quiero agregar un producto a mi lista de favoritos
- Como cliente, quiero calificar un producto de 1 a 5
- Como cliente, quiero comentar acerca un producto que compre
- Como cliente, quiero preguntar acerca de un producto
- Como administrador, quiero responder a preguntas de clientes
- Como administrador, quiero ver los diversos movimientos dentro del sistema

## Arquitectura de software

El sistema será web, para permitir la expansión de la empresa pudiendo realizar ventas a diferentes localidades.

## Tecnologia a utilizar

- Lenguaje: PHP 7.4
- Framework: Laravel 8
- Arquitectura: Cliente/Servidor
- Motor de Base de Datos: MySQL 5.7
- Versionado: GitLab
