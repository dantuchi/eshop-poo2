# Miembros del Equipo
- Cubilla Mauro
- Pluhator Nicolas

# E-shop Tienda Informática

Este es un proyecto desarrollado para la materia Programación orientada a objetos 2, correspondiente a la carrera de Licenciatura en Sistemas de Información de la FCEQyN, UNaM.

## Descripción breve del proyecto

Se desarrollará un sistema web, el cual será un e-shop de insumos informáticos donde se le permitirá a los clientes poder realizar compras de artículos informáticos vía online, como también registrar compras llevadas a cabo en el local de la empresa, y además llevar a cabo toda la gestión relacionada al abastecimiento de los insumos de la empresa.


